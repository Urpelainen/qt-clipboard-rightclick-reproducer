## Qt Clipboard rightclick reproducer

Minimal reproducer for a Qt bug where right click clears the clipboard.

## Preconditions

1. GNOME
2. Wayland

## Usage

Instructions testing for Fedora 34:

Build and run the example:
```
cd Qt6Example
qt6-qmake
make
QT_QPA_PLATFORM=wayland ./Example
```

Copy some text to clipboard from another application.

Copy text to editline with Ctrl-V, observe success.

Right click.

Copy text to editline with Ctrl-V, observe that nothing happens.

Same happens on Qt5 also, in folder `Qt5Example`.

## WAYLAND_DEBUG=1

When WAYLAND_DEBUG=1 is used, the following is seen on the log:

```
[1228388,321] xdg_toplevel@26.configure(306, 333, array)
[1228388,600] xdg_surface@25.configure(2073)
[1228388,725] wl_keyboard@7.modifiers(33425, 0, 0, 16, 0)
[1228389,063] wl_keyboard@7.enter(33425, wl_surface@21, array)
[1228389,282] wl_data_device@13.data_offer(new id wl_data_offer@37903216)
[1228389,371] wl_data_offer@4278190080.offer("text/plain")
[1228389,408] wl_data_offer@4278190080.offer("text/plain;charset=utf-8")
[1228389,436] wl_data_offer@4278190080.offer("STRING")
[1228389,463] wl_data_offer@4278190080.offer("TEXT")
[1228389,489] wl_data_offer@4278190080.offer("COMPOUND_TEXT")
[1228389,515] wl_data_offer@4278190080.offer("UTF8_STRING")
[1228389,541] wl_data_device@13.selection(wl_data_offer@4278190080)
[1228389,573] zwp_primary_selection_device_v1@14.data_offer(new id zwp_primary_selection_offer_v1@42416656)
[1228389,620] zwp_primary_selection_offer_v1@4278190082.offer("UTF8_STRING")
[1228389,648] zwp_primary_selection_device_v1@14.selection(zwp_primary_selection_offer_v1@4278190082)
[1228389,696] xdg_wm_base@24.ping(32724185)
```

Looks like right click pushes something to clipboard.
